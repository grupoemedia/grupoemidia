<?php
/**
 * Functions - Child theme custom functions
 */


/*****************************************************************************************************************
************************** Caution: do not remove or edit anything within this section **************************/

/**
 * Loads the Divi parent stylesheet.
 * Do not remove this or your child theme will not work unless you include a @import rule in your child stylesheet.
 */
function the_asset($uri) {
    echo get_theme_file_uri('assets/' . $uri);
}
function dce_load_divi_stylesheet() {
    wp_enqueue_style( 'divi-parent-style', get_template_directory_uri() . '/style.css' );


    wp_enqueue_style( 'custom-css', get_theme_file_uri('assets/css/custom.css') );
    wp_enqueue_script( 'custom-js', get_theme_file_uri('assets/css/custom.js') );


    wp_enqueue_script( 'custom1.1-js',  'http://code.jquery.com/jquery-1.10.0.min.js' ); 
    wp_enqueue_style( 'tooltip-css', get_theme_file_uri('assets/tooltipster/dist/css/tooltipster.bundle.min.css') );
    wp_enqueue_style( 'tooltip-style-css', get_theme_file_uri('assets/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css') );
    wp_enqueue_script( 'tooltip-js', get_theme_file_uri('assets/tooltipster/dist/js/tooltipster.bundle.min.js') );
	
    
    
    
}
add_action( 'wp_enqueue_scripts', 'dce_load_divi_stylesheet' );

/**
 * Makes the Divi Children Engine available for the child theme.
 * Do not remove this or you will lose all the customization capabilities created by Divi Children Engine.
 */
require_once('divi-children-engine/divi_children_engine.php');

/****************************************************************************************************************/


/**
 * Patch to fix Divi issue: Duplicated Predefined Layouts.
 */
if ( function_exists( 'et_pb_update_predefined_layouts' ) ) {
	remove_action( 'admin_init', 'et_pb_update_predefined_layouts' );
	function Divichild_pb_update_predefined_layouts() {
			if ( 'on' === get_theme_mod( 'et_pb_predefined_layouts_updated_2_0' ) ) {
				return;
			}
			if ( ! get_theme_mod( 'et_pb_predefined_layouts_added' ) OR ( 'on' === get_theme_mod( 'et_pb_predefined_layouts_added' ) )) {	
				et_pb_delete_predefined_layouts();
			}
			et_pb_add_predefined_layouts();
			set_theme_mod( 'et_pb_predefined_layouts_updated_2_0', 'on' );
	}
	add_action( 'admin_init', 'Divichild_pb_update_predefined_layouts' );
}

function category_Loop() {
	ob_start();

	get_template_part('shortcodes/category/categoryLoop', '');

 	return ob_get_clean();
}
add_shortcode( 'categoryLoop', 'category_Loop' );

function category_Loop2() {
	ob_start();

	get_template_part('shortcodes/category/categoryLoop2', '');

 	return ob_get_clean();
}
add_shortcode( 'categoryLoop2', 'category_Loop2' );

function relationship_Posts() {
	$a = shortcode_atts( array(
        'excludeId' => null,
        'amount' => 3
    ), $atts );

	$info='';

	if (is_single()) {
		$a['excludeId'] = get_the_ID();
	}

	$cat = get_the_category($a['excludeId']);


    $args = array( 'category_name' => $cat[0]->slug, 'post__not_in'=>array($a['excludeId']), 'order'=>'DESC', 'orderby'=>'date', 'posts_per_page'=>$a['amount']); 

    global $relationshipPosts;
    $relationshipPosts = get_posts($args);

	ob_start();

	get_template_part('shortcodes/posts/relationshipPosts', '');

 	return ob_get_clean();
}
add_shortcode( 'relationshipPosts', 'relationship_Posts' );

function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}

function create_posttype() {
    register_post_type( 'portfolios',
        array(
            'menu_icon' => 'dashicons-format-image',
            'menu_position' => 5,
            'labels' => array(
                'name' => __( 'Portfolios' ),
                'singular_name' => __( 'Portfolio' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'portfolios', 'with_front' => false),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
            ),
        )
    );
    register_post_type( 'materiais-educativos',
        array(
            'menu_icon' => 'dashicons-welcome-learn-more',
            'menu_position' => 5,
            'labels' => array(
                'name' => __( 'Materiais Educativos' ),
                'singular_name' => __( 'Material Educativo' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'materiais-educativos', 'with_front' => false),
            'supports' => array(
                'title',
                'thumbnail',
            ),

        )
    );
    register_taxonomy('formato_material','materiais-educativos',array(
        'hierarchical' => true,
        'labels' => [
            'name' => 'Formatos',
            'singular_name' => 'Formato',
            'search_items' =>  'Buscar formatos',
            'popular_items' => 'Formatos populares',
            'all_items' => 'Todas os formatos',
            'edit_item' => 'Editar formatos',
            'update_item' => 'Atualizar formatos',
            'add_new_item' => 'Adicionar novo',
            'new_item_name' => 'Novo formato',
            'separate_items_with_commas' => 'Separe os formatos por vírgula',
            'add_or_remove_items' => 'Adicione ou remova formatos',
            'choose_from_most_used' => 'Selecione entre os formatos mais utilizados',
            'menu_name' => 'Formatos',
        ],
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'formato_material' ),
    ));
}
add_action('init', 'create_posttype');

function cpt_Loop() {

    ob_start();

    get_template_part('shortcodes/posts/cptLoop', '');

    return ob_get_clean();
   
}
add_shortcode( 'cptLoop', 'cpt_loop' );

// Add our custom permastructures for custom taxonomy and post
add_action( 'wp_loaded', 'add_clinic_permastructure' );
function add_clinic_permastructure() {
    global $wp_rewrite;
    add_permastruct( 'formato_material', 'materiais-educativos/%formato_material%', false );
    add_permastruct( 'materiais-educativos', 'materiais-educativos/%formato_material%/%slug%', false );
}


// Make sure that all links on the site, include the related texonomy terms
add_filter( 'post_type_link', 'inspirese_cat_permalinks', 10, 2 );
function inspirese_cat_permalinks( $permalink, $post ) {
    if ( $post->post_type !== 'inspirese' )
        return $permalink;
    $terms = get_the_terms( $post->ID, 'formato_material' );

    if ( ! $terms )
        return str_replace( '%formato_material%/', '', $permalink );
    $post_terms = array();
    foreach ( $terms as $term )
        $post_terms[] = $term->slug;
    return str_replace( '%formato_material%', implode( ',', $post_terms ) , $permalink );
}


// Make sure that all term links include their parents in the permalinks
add_filter( 'term_link', 'add_term_parents_to_permalinks', 10, 2 );
function add_term_parents_to_permalinks( $permalink, $term ) {
    $term_parents = get_term_parents( $term );

    foreach ( $term_parents as $term_parent )
        $permalink = str_replace( $term->slug, $term_parent->slug . ',' . $term->slug, $permalink );

    return $permalink;
}


// Helper function to get all parents of a term
function get_term_parents( $term, &$parents = array() ) {
    $parent = get_term( $term->parent, $term->taxonomy );

    if ( is_wp_error( $parent ) )
        return $parents;

    $parents[] = $parent;
    if ( $parent->parent )
        get_term_parents( $parent, $parents );
    return $parents;
}
function taxonomy_Loop() {

    ob_start();

    get_template_part('shortcodes/category/taxonomyLoop', '');

    return ob_get_clean();
   
}
add_shortcode( 'taxonomyLoop', 'taxonomy_Loop' );

function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchForm" class="searchForm" action="' . home_url( '/materiais-educativos/' ) . '" >
    <div class="searchDiv">
    <input class="searchInput" type="text" placeholder="Encontre um material didático..." value="' . get_search_query() . '" name="s" id="s" />
    <input class="searchButton" type="submit" id="searchsubmit" value="'. esc_attr__( 'Buscar' ) .'" />
    </div>
    </form>';
 
    return $form;
}
add_shortcode( 'getSearchForm', 'wpdocs_my_search_form' );