<?php 
$categories = get_categories();
?>

<ul class="categoryList2">
	<?php foreach ($categories as $category): ?>
		<li>
		  	<a href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->cat_name ?></a>
		</li>
	<?php endforeach; ?>
</ul>

