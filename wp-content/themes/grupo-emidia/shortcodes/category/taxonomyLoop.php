<?php 

	$terms = get_terms( array(
	    'taxonomy' => 'formato_material',
	    'hide_empty' => false,
	) );
?>
	<ul>

		<li>
			<a href="<?php echo get_site_url(); ?>/materiais-educativos">

			Todos

			</a>
		</li>

<?php
	foreach ($terms as $term) { ?>
		<li>
			<a href="<?php echo get_term_link( $term ); ?>">

			<?php echo $term->name ?>

			</a>
		</li>
		<?php
	}
?>	
	</ul>
<?php


