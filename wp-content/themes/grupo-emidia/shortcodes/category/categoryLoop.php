<?php 
$categories = get_categories();
?>

<nav class="categorias">
	<ul class="categoryList">
			<li>
				<a class="categoryLink" href="<?php echo get_site_url() ?>/blog">
			  		<img class="tooltip" title="Todos" src="<?php echo get_template_directory_uri(); ?>/../grupo-emidia/images/grid.png"/>
			  	</a>
			</li>
		<?php foreach ($categories as $category): ?>
			<li>
			  	<a class="categoryLink" href="<?php echo get_category_link($category->term_id) ?>">
			  		<img class="tooltip" title="<?php echo $category->cat_name ?>" src="<?php echo z_taxonomy_image_url($category->term_id) ?>" />,
			  	</a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>


