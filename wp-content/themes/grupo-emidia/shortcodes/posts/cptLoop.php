<?php global $loop;


$queried_object = get_queried_object();
 
 if ($queried_object instanceof \WP_Term) {
        $loop = new WP_Query( array( 
            's' => 'Planejamento',
            'post_type' => 'materiais-educativos', 
            'posts_per_page' => 24,
            'tax_query' => array(
                array (
                    'taxonomy' => $queried_object->taxonomy,
                    'field' => 'slug',
                    'terms' => $queried_object->slug,
                ),
            ),


    ) );
} else {
    $loop = new WP_Query( array( 'post_type' => 'materiais-educativos', 'posts_per_page' => 24, 'category' => 'current', 's' => $_GET['s']) );
}

    
?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
    <div class="materiais-educativos et_pb_column et_pb_column_1_3">
        <div class="materiais-image">
            <a href="<?php the_field('link', get_the_ID()); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('full');} ?></a>
        </div>
        <div class="materiais-formato">
        	<h4><?php echo implode(', ', wp_get_post_terms(get_the_ID(), 'formato_material', array("fields" => "names"))); ?></h4>
        </div>
        <div class="materiais-title">
            <h3><?php echo get_the_title(); ?></h3>
        </div>
    </div>

<?php endwhile; wp_reset_query();