<?php global $relationshipPosts; ?>
<?php foreach($relationshipPosts as $post): ?>

	<div class="et_pb_column et_pb_column_1_3">
		<div class="categoryItem et_pb_blurb_content">
			<a class="relationshipPost" href="<?php echo get_permalink($post) ?>">
			<?php echo get_the_post_thumbnail($post, 'full') ?>
			<h2 class="relationshipName"><?php echo get_the_title($post)?></h2>
			<?php echo apply_filters('the_excerpt', $post->post_excerpt)?>
			</a>
		</div>
	</div>


<?php endforeach; ?>