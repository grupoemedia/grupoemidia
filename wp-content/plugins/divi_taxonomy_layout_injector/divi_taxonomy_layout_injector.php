<?php

/*
 * Plugin Name: Taxonomy Layout Injector
 * Plugin URI:  http://www.sean-barton.co.uk
 * Description: A plugin to handle the layouts of archive, category and taxonomy pages using the Divi Builder
 * Author:      Sean Barton - Tortoise IT
 * Version:     1.9
 * Author URI:  http://www.sean-barton.co.uk
 *
 *
 * Changelog:
 * 1.6
 * - Bug fix. Removed issue whereby a site with LOTs of users wouldn't be able to save layouts. Limited author layout selection to certain roles
 * - Feature. Added post date archive layout option
 *
 * V1.7
 * - Fixed responsive text size/line height issues
 * - Added more configuration options in advanced design settings across all modules
 * - Added Taxonomy Loop Archive module for far greater control over your archive pages
 * - Added title, gallery and content modules to work in conjunction with the loop archive module added also in this version
 *
 * V1.8 - 10/2/17
 * - Added licensing and auto updates
 * - Tidied up the settings page to make it more user friendly with handy links to Youtube and documentation
 * - Removed superfluous layouts from all layout dropdowns on the settings page
 * - Fixed grid columns setting in Loop Archive module showing even when full width was selected
 * - Added text colour and alignment options to title and content modules for ease of use
 *
 * V1.9 - 22/2/17
 * - Added option to hide module if no results in loop archive
 * - Removed error around show categories in the archive module
 *
 */
		require_once('includes/emp-licensing.php');
 
		define('SB_ET_TAX_LI_VERSION', '1.9');
 
    add_action('plugins_loaded', 'sb_et_tax_li_init');
    
    function sb_et_tax_li_init() {
    
        add_action('admin_menu', 'sb_et_tax_li_submenu');
        add_action('init', 'sb_et_tax_li_theme_setup', 9999);
        add_action('admin_head', 'sb_et_tax_li_admin_head', 9999);
        add_action('wp_enqueue_scripts', 'sb_et_tax_li_enqueue', 9999);
        
        add_filter( 'template_include', 'sb_et_tax_li_template', 99 );
        
    }
    
    function sb_et_tax_li_enqueue() {
        wp_enqueue_style('sb_et_tax_li_css', plugins_url( '/style.css', __FILE__ ));
    }
    
    function sb_et_tax_li_admin_head() {
        
        if (!isset($_GET['post_type']) || $_GET['post_type'] != 'et_pb_layout' || stripos($_SERVER['PHP_SELF'], 'wp-admin/index.php') !== false) {
						return; //we will only purge the cache on the field groups page
				}
            
        $prop_to_remove = array(
            'et_pb_templates_et_pb_tax_loop_archive'
            , 'et_pb_templates_et_pb_tax_user_avatar'
            , 'et_pb_templates_et_pb_tax_archive'
            , 'et_pb_templates_et_pb_tax_post_title'
            , 'et_pb_templates_et_pb_tax_title'
            , 'et_pb_templates_et_pb_tax_text'
            , 'et_pb_templates_et_pb_tax_featured_image'
        );
        
        $js_prop_to_remove = 'var sb_ls_remove = ["' . implode('","', $prop_to_remove) . '"];';

        echo '<script>
        
        ' . $js_prop_to_remove . '
        
        for (var prop in localStorage) {
            if (sb_ls_remove.indexOf(prop) != -1) {
                console.log(localStorage.removeItem(prop));
            }
        }
        
        </script>';
    } 
    
    function sb_et_tax_li_get_layout() {
        $layouts = get_option('sb_et_tax_li');
        $object = get_queried_object();
				$layout = '';
        
        //echo '<pre>';
        //print_r($object);
        //echo '</pre>';
        
        if (is_author()) {
            if (!$layout = $layouts['author_archives']['terms'][$object->slug]) {
                $layout = $layouts['author_archives']['archive'];
            }
        } else if (is_date()) {
						$layout = $layouts['date_archives']['archive'];
        } else {
						if (isset($object->taxonomy) && $object->taxonomy) {
								if (!$layout = $layouts[$object->taxonomy]['terms'][$object->slug]) {
										$layout = $layouts[$object->taxonomy]['archive'];
								}
						}
        }
        
        return $layout;
    }
    
    function sb_et_tax_li_template( $template ) {
        
        if (is_archive()) {
            $layout = sb_et_tax_li_get_layout();
            
            $admin_mode = get_option('sb_et_tax_li_admin_mode');
            $is_admin = current_user_can('administrator');
            
            if (!$admin_mode || ($admin_mode && $is_admin)) {
                if ($layout) {
                    $template = dirname(__FILE__) . '/empty.php'; //stops woo from taking over the layout
                }
            }
        }
    
        return $template;
    }
    
    function sb_et_tax_li_single_template() {
        get_header();
        
        $layout = sb_et_tax_li_get_layout();
        
        if ($layout) {
            
            if ($section = do_shortcode('[et_pb_section global_module="' . $layout . '"][/et_pb_section]')) {
                echo '<div id="main-content">';
                echo $section;
                echo '</div>';
            }
        }
        
        get_footer();
    }
 
    function sb_et_tax_li_submenu() {
        add_submenu_page(
            'options-general.php',
            'Taxonomy Layout Injector',
            'Taxonomy Layout Injector',
            'manage_options',
            'sb_et_tax_li',
            'sb_et_tax_li_submenu_cb' );
    }
    
    function sb_et_tax_li_box_start($title) {
        return '<div class="postbox">
                    <h2 class="hndle">' . $title . '</h2>
                    <div class="inside">';
    }
    
    function sb_et_tax_li_box_end() {
        return '    <div style="clear: both;"></div></div>
                </div>';
    }
     
    function sb_et_tax_li_get_taxonomies() {
            $return = array();
                    
            $ignored = array(
                'nav_menu'
                , 'post_format'
                , 'link_category'
            );
            
            $ignored_stubs = array(
                'pa_'
                , 'product_'
            );
            
            if ($taxonomies = get_taxonomies(false, 'object')) {
                foreach ( $taxonomies  as $taxonomy ) {
                    if ($taxonomy->publicly_queryable && !in_array($taxonomy->name, $ignored)) {
                        $ignore_this = false;
                    
                        foreach ($ignored_stubs as $ignored_stub) {
                            $length = strlen($ignored_stub);
                            if (substr($taxonomy->name, 0, $length) == $ignored_stub) {
                                $ignore_this = true;
                                break;
                            }
                        }
                    
                        if (!$ignore_this) {
                            $return[] = $taxonomy;
                        }
                    }
                }
            }
            
            return $return;
    }
    
    function sb_et_tax_li_submenu_cb() {
        
        echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
        echo '<h2>Taxonomy Layout Injector - V' . SB_ET_TAX_LI_VERSION . '</h2>';
        
        echo '<div id="poststuff">';
        
        echo '<div id="post-body" class="metabox-holder columns-2">';
				
				//echo '<pre>';
				//print_r($_POST);
				//echo '</pre>';
        
        if (isset($_POST['sb_et_tax_li'])) {
						//echo 'updating';
            update_option('sb_et_tax_li', $_POST['sb_et_tax_li']);
            update_option('sb_et_tax_li_admin_mode', $_POST['sb_et_tax_li_admin_mode']);
            
            echo '<div id="message" class="updated fade"><p>Layouts edited successfully</p></div>';
        }
        
        $layout_saved = get_option('sb_et_tax_li');
				
				$taxonomies = sb_et_tax_li_get_taxonomies();
        
        //echo '<pre>';
        //print_r($layout_saved);
        //echo '</pre>';
        
        echo '<p>This plugin allows you to edit the layouts of your Divi site without having to edit any core files. See each section below for more information</p>';
        
        echo '<form method="POST">';
				
				sb_et_tax_li_license_page();
        
        $layout_query = array(
            'post_type'=>'et_pb_layout'
            , 'posts_per_page'=>-1
            , 'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );
        
        echo sb_et_tax_li_box_start('Taxonomy/Author Archive Layout Injector');
        
        echo '<div class="alignright"><iframe width="300" height="169" src="https://www.youtube.com/embed/hMMWhi2sIlI?rel=0" frameborder="0" allowfullscreen></iframe></div>';
        
        echo '<p>This plugin allows you to edit the layouts of your Divi site without having to edit any core files.</p>';
				
        echo '<p>A layout can be built within the Divi/Extra library using the page builder. You can then use these settings to set the appropriate layout to your taxonomy/archive page(s). You\'ll need to include a variety of new modules in the page builder to make the page work. This plugin will do the rest!</p><p>If you need any support for this plugin please visit the documentation website at <a href="http://docs.tortoise-it.co.uk/taxonomy-layout-injector/" target="_blank">docs.tortoise-it.co.uk/taxonomy-layout-injector/</a>.</p><p>The following video will explain the loop archive module which is integral to this plugin.</p>';
        
        echo sb_et_tax_li_box_end();
        
        echo '<div style="display: inline-block; margin-right: 10px; margin-bottom: 20px; padding: 5px 0px;">Jump to:</div>';
                
        foreach ($taxonomies as $type2) {
            $type_name2 = $type2->labels->name;
						
            echo '<div style="display: inline-block; margin-right: 10px; margin-bottom: 20px; padding: 5px 10px; background: #333"><a style="color: white; text-decoration: none; " href="#' . $type2->name . '">' . $type_name2 . '</a></div>';
        }				
        
        if ($layouts = get_posts($layout_query)) {
            
            echo sb_et_tax_li_box_start('General Settings');
        
            echo '<p>
                <label><input type="checkbox" name="sb_et_tax_li_admin_mode" ' . checked(1, get_option('sb_et_tax_li_admin_mode', 0), false) . ' value="1" /> Admin test mode?</label>
                <br /><small>Using admin test mode the layout will only be applied for admin users. Non-admin users will see the default product page layouts.</small>
            </p>';
            
            echo sb_et_tax_li_box_end();

            if ($taxonomies) {
                foreach ($taxonomies as $taxonomy) {
										
                    $terms = get_terms( $taxonomy->name, array(
                        'hide_empty' => false,
                    ) );
                    
                    if ($terms) {
                        echo '<a name="' . $taxonomy->name . '"></a>';
                        echo sb_et_tax_li_box_start($taxonomy->labels->name);
                    
                        echo 'Archive Layout: <select name="sb_et_tax_li[' . $taxonomy->name . '][archive]">';
                        
                            echo '<option value="">-- None --</option>';
                            
                            foreach ($layouts as $layout) {
                                echo '<option ' . selected($layout->ID, $layout_saved[$taxonomy->name]['archive'], false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                            }
                
                        echo '</select>';
                        
												if (count($terms) < 100) {
														echo '<p><strong>Individual Term Overrides (<a style="cursor: pointer;" onclick="jQuery(\'.term_overrides_' . $taxonomy->name . '\').slideToggle();">toggle terms</a>)</strong></p>';
														
														echo '<div class="term_overrides_' . $taxonomy->name . '" style="display: none;">';
														
														echo '<p>The above layout is global but selecting layouts using the category list below will override the first setting and use those instead. EG. If you had a category called books you might want to include a longer description and maybe a chapter whereas selling CDs might require a track listing. In this circumstance you would want to use a different layout for each category. Using the boxes below allows you to do that.</p>';
														
														echo '<table style="width: 100%;" class="widefat">';
																
														foreach ($terms as $term) {
																
																echo '<tr><td>' . $term->name . '</td><td>';
																echo '<select name="sb_et_tax_li[' . $taxonomy->name . '][terms][' . $term->slug . ']">';
																
																		echo '<option value="">-- Default --</option>';
																		
																		foreach ($layouts as $layout) {
																				echo '<option ' . selected($layout->ID, $layout_saved[$taxonomy->name]['terms'][$term->slug], false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
																		}
												
																echo '</select>';
																echo '</td></tr>';
																
														}
														
														echo '</table>';
														
														echo '</div>';
												} else {
														echo '<p>You would normally be able to override the layout of each term in this taxonomy but because you have more than 100 items this will not be possible due to server limitations. If you would like this functionality reinstated please contact the <a href="http://www.sean-barton.co.uk/support">plugin author</a> for help.</p>';
												}
												
                        echo sb_et_tax_li_box_end();
                    }
                    
                }
            }
            
            echo sb_et_tax_li_box_start('Author Pages');
                    
            echo 'Authors Layout: <select name="sb_et_tax_li[author_archives][archive]">';
            
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $layout_saved['author_archives']['archive'], false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
            echo '<p><strong>Individual Author Overrides (<a style="cursor: pointer;" onclick="jQuery(\'.term_overrides_author_archives\').slideToggle();">toggle terms</a>)</strong></p>';
            
            echo '<div class="term_overrides_author_archives" style="display: none;">';
            
            echo '<p>The above layout is global but selecting layouts using the author list below will override the first setting and use those instead.</p>';
            
            echo '<table style="width: 100%;" class="widefat">';
                
            $authors = get_users(array('role__not_in'=>array('subscriber', 'customer', 'bbp_participant')));
            foreach ($authors as $author) {
                
                echo '<tr><td>' . $author->user_login . '</td><td>';
                echo '<select name="sb_et_tax_li[author_archives][terms][' . $author->user_login . ']">';
                
                    echo '<option value="">-- Default --</option>';
                    
                    foreach ($layouts as $layout) {
                        echo '<option ' . selected($layout->ID, $layout_saved['author_archives']['terms'][$author->user_login], false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                    }
        
                echo '</select>';
                echo '</td></tr>';
                
            }
            
            echo '</table>';
            
            echo '</div>';
            
            echo sb_et_tax_li_box_end();
						
						echo sb_et_tax_li_box_start('Post Date Archive');
                    
            echo 'Date Archive Layout: <select name="sb_et_tax_li[date_archives][archive]">';
            
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $layout_saved['date_archives']['archive'], false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
    
            echo '</select>';
            
            echo '</div>';
            
            echo sb_et_tax_li_box_end();
						
				} else {
            echo '<div style="padding:100px; border: 2px solid #999; text-align: center;">
                    <h1>Oops no layouts!</h1>
                    <p style="font-size: 16px;">Please visit the Divi Library to add your first layout and then this page will become available</p>
                    <p><a href="' . (admin_url('/edit.php?post_type=et_pb_layout')) . '" style="display: inline-block; padding: 10px 30px; border: 1px solid #999; font-size: 16px; background-color: #333; color: white; font-weight: bold; border-radius: 20px; text-decoration: none;">Click here to visit the Divi Library</a></p>
                </div>';
        }
        
        echo '<p id="submit"><input type="submit" name="sb_et_tax_li_edit_submit" class="button-primary" value="Save Settings" /></p>';
        
        echo '</form>';
            
        echo '</div>';
        
        echo '</div>';
        echo '</div>';
				
    }
    
    function sb_et_tax_li_theme_setup() {
    
        if ( class_exists('ET_Builder_Module')) {
            
            $include = true;
          
            if ($include) {
                $modules_path = trailingslashit(dirname(__FILE__)) . 'modules/';
                
                require_once($modules_path . 'sb_et_tax_li_tax_title_module.php');
                require_once($modules_path . 'sb_et_tax_li_avatar_module.php');
                require_once($modules_path . 'sb_et_tax_li_title_module.php');
                require_once($modules_path . 'sb_et_tax_li_gallery_module.php');
                require_once($modules_path . 'sb_et_tax_li_content_module.php');
                require_once($modules_path . 'sb_et_tax_li_loop_archive_module.php');
                require_once($modules_path . 'sb_et_tax_li_archive_module.php');
            }
            
        }
    }
 
?>