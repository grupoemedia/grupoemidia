<?php

class sb_et_tax_li_avatar_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'ET User Avatar', 'et_builder' );
                    $this->slug = 'et_pb_tax_user_avatar';
            
                    $this->whitelisted_fields = array(
                        'avatar_size',
                        'animation',
                        'module_id',
                        'module_class',
                    );
            
                    $this->fields_defaults = array('avatar_size'=>256);
                    $this->main_css_element = '.et_pb_tax_user_avatar';
                    $this->advanced_options = array(
                                        'background' => array(
                                                'settings' => array(
                                                        'color' => 'alpha',
                                                ),
                                        ),
                                        'border' => array(),
                                        'custom_margin_padding' => array(
                                                'css' => array(
                                                        'important' => 'all',
                                                ),
                                        ),
                                );
                    $this->custom_css_options = array();
                }
            
                function get_fields() {
                                
                                // List of animation options
                        $animation_options = array(
                                'left'    => esc_html__( 'Left To Right', 'et_builder' ),
                                'right'   => esc_html__( 'Right To Left', 'et_builder' ),
                                'top'     => esc_html__( 'Top To Bottom', 'et_builder' ),
                                'bottom'  => esc_html__( 'Bottom To Top', 'et_builder' ),
                                'fade_in' => esc_html__( 'Fade In', 'et_builder' ),
                                'off'     => esc_html__( 'No Animation', 'et_builder' ),
                        );
                        
                    $fields = array(
                                'avatar_size' => array(
                                    'label'       => __( 'Avatar Size', 'et_builder' ),
                                    'type'        => 'text',
                                    'description' => __( 'Set the size of the Avatar. If using this in a 1/4 column then around 256 would work. If using a 1/3 then you\'ll need to go to around 320. If you choose a size larger than the column, it\'s ok because the avatar image will never be wider and it will still work responsively', 'et_builder' ),
                                ),
                                'animation' => array(
                                        'label'             => esc_html__( 'Animation', 'et_builder' ),
                                        'type'              => 'select',
                                        'option_category'   => 'configuration',
                                        'options'           => $animation_options,
                                        'description'       => esc_html__( 'This controls the direction of the lazy-loading animation.', 'et_builder' ),
                                ),
                                'admin_label' => array(
                                    'label'       => __( 'Admin Label', 'et_builder' ),
                                    'type'        => 'text',
                                    'description' => __( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                                ),
                                'module_id' => array(
                                    'label'           => __( 'CSS ID', 'et_builder' ),
                                    'type'            => 'text',
                                    'option_category' => 'configuration',
                                    'description'     => __( 'Enter an optional CSS ID to be used for this module. An ID can be used to create custom CSS styling, or to create links to particular sections of your page.', 'et_builder' ),
                                ),
                                'module_class' => array(
                                    'label'           => __( 'CSS Class', 'et_builder' ),
                                    'type'            => 'text',
                                    'option_category' => 'configuration',
                                    'description'     => __( 'Enter optional CSS classes to be used for this module. A CSS class can be used to create custom CSS styling. You can add multiple classes, separated with a space.', 'et_builder' ),
                                ),
                    );
                    
                    return $fields;
                }
            
                function shortcode_callback( $atts, $content = null, $function_name ) {
                    $module_id          = $this->shortcode_atts['module_id'];
                    $module_class       = $this->shortcode_atts['module_class'] . ' et-waypoint';
                    $animation       = $this->shortcode_atts['animation'];
                    $output = '';
                    
                    if (!$avatar_size = $this->shortcode_atts['avatar_size']) {
                                $avatar_size = 256;
                    }
            
                    $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
            
                    //////////////////////////////////////////////////////////////////////
                    
                    if (is_author()) {
                                $content = get_avatar(get_the_author_meta('id'), $avatar_size);
                      
                                //////////////////////////////////////////////////////////////////////
            
                                $output = sprintf(
                                    '<div%5$s class="%1$s%3$s%6$s">
                                        %2$s
                                    %4$s',
                                    'clearfix ',
                                    $content,
                                    esc_attr( 'et_pb_module et_pb_animation_' . $animation ),
                                    '</div>',
                                    ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                                    ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
                                );
                    }
            
                    return $output;
                }
            }
        
            new sb_et_tax_li_avatar_module();

?>