<?php

class sb_et_tax_li_tax_title_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'ET Taxonomy/Archive Title', 'et_builder' );
                    $this->slug = 'et_pb_tax_title';
            
                    $this->whitelisted_fields = array(
                                'background_layout',
																'text_orientation',
                                'show_desc',
                                'avatar_size',
                                'show_avatar',
                                'module_id',
                                'module_class',
                                'max_width',
                                'max_width_tablet',
                                'max_width_phone',
                    );
            
                    $this->fields_defaults = array(
                                                'background_layout' => array( 'light' ),
                                                'text_orientation'  => array( 'left' ),
                                );
                    
                    $this->main_css_element = '%%order_class%%';
                    
                $this->advanced_options = array(
                                'fonts' => array(
                                                'text'   => array(
                                                                'label'    => esc_html__( 'Text', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} p",
                                                                ),
                                                                'font_size' => array('default' => '14px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                                'headings'   => array(
                                                                'label'    => esc_html__( 'Headings', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} h1, {$this->main_css_element} h2, $this->main_css_element} h1 a, {$this->main_css_element} h2 a, {$this->main_css_element} h3, {$this->main_css_element} h4",
                                                                ),
                                                                'font_size' => array('default' => '30px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                ),
                                'background' => array(
                                        'settings' => array(
                                                'color' => 'alpha',
                                        ),
                                ),
                                'border' => array(),
                                'custom_margin_padding' => array(
                                        'css' => array(
                                                'important' => 'all',
                                        ),
                                ),
                );                
                
                $this->custom_css_options = array();
                }
            
                function get_fields() {
                    $fields = array(
                                'background_layout' => array(
                                                'label'             => esc_html__( 'Text Color', 'et_builder' ),
                                                'type'              => 'select',
                                                'option_category'   => 'configuration',
                                                'options'           => array(
                                                  'light' => esc_html__( 'Dark', 'et_builder' ),
                                                  'dark'  => esc_html__( 'Light', 'et_builder' ),
                                                ),
                                                'description'       => esc_html__( 'Here you can choose the value of your text. If you are working with a dark background, then your text should be set to light. If you are working with a light background, then your text should be dark.', 'et_builder' ),
                                ),
                                'text_orientation' => array(
                                                'label'             => esc_html__( 'Text Orientation', 'et_builder' ),
                                                'type'              => 'select',
                                                'option_category'   => 'layout',
                                                'options'           => et_builder_get_text_orientation_options(),
                                                'description'       => esc_html__( 'This controls the how your text is aligned within the module.', 'et_builder' ),
                                ),
                                'show_desc' => array(
                                                'label'           => __( 'Show Description?', 'et_builder' ),
                                                'type'            => 'yes_no_button',
                                                'option_category' => 'configuration',
                                                'options'         => array(
                                                                'off' => __( 'No', 'et_builder' ),
                                                                'on'  => __( 'Yes', 'et_builder' ),
                                                ),
                                                'description'       => __( 'Should this show the category description/author bio?', 'et_builder' ),
                                ),
                                'show_avatar' => array(
                                                'label'           => __( 'Show Avatar?', 'et_builder' ),
                                                'type'            => 'yes_no_button',
                                                'option_category' => 'configuration',
                                                'options'         => array(
                                                                'off' => __( 'No', 'et_builder' ),
                                                                'on'  => __( 'Yes', 'et_builder' ),
                                                ),
                                                'affects'           => array(
                                                                '#et_pb_avatar_size',
                                                ),
                                                'description'       => __( 'Should this show the gravatar be shown for author pages?', 'et_builder' ),
                                ),
                                'avatar_size' => array(
                                    'label'       => __( 'Avatar Size', 'et_builder' ),
                                    'type'        => 'text',
                                    'depends_show_if'   => 'on',
                                    'description' => __( 'This will set the size of the Avatar. ', 'et_builder' ),
                                ),
                                'admin_label' => array(
                                    'label'       => __( 'Admin Label', 'et_builder' ),
                                    'type'        => 'text',
                                    'description' => __( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                                ),
                                'module_id' => array(
                                    'label'           => __( 'CSS ID', 'et_builder' ),
                                    'type'            => 'text',
                                    'option_category' => 'configuration',
                                    'description'     => __( 'Enter an optional CSS ID to be used for this module. An ID can be used to create custom CSS styling, or to create links to particular sections of your page.', 'et_builder' ),
                                ),
                                'module_class' => array(
                                    'label'           => __( 'CSS Class', 'et_builder' ),
                                    'type'            => 'text',
                                    'option_category' => 'configuration',
                                    'description'     => __( 'Enter optional CSS classes to be used for this module. A CSS class can be used to create custom CSS styling. You can add multiple classes, separated with a space.', 'et_builder' ),
                                ),
                                'max_width' => array(
                                                'label'           => esc_html__( 'Max Width', 'et_builder' ),
                                                'type'            => 'text',
                                                'option_category' => 'layout',
                                                'mobile_options'  => true,
                                                'tab_slug'        => 'advanced',
                                                'validate_unit'   => true,
                                ),
                                'max_width_tablet' => array(
                                                'type'      => 'skip',
                                                'tab_slug'  => 'advanced',
                                ),
                                'max_width_phone' => array(
                                                'type'      => 'skip',
                                                'tab_slug'  => 'advanced',
                                ),
                            );
                    
                    return $fields;
                }
            
                function shortcode_callback( $atts, $content = null, $function_name ) {
                                $content = '';
                                
                                $module_id          = $this->shortcode_atts['module_id'];
                                $module_class       = $this->shortcode_atts['module_class'];
                                $show_desc       = $this->shortcode_atts['show_desc'];
                                $show_avatar       = $this->shortcode_atts['show_avatar'];
                                $background_layout    = $this->shortcode_atts['background_layout'];
                                $text_orientation     = $this->shortcode_atts['text_orientation'];
                                $max_width            = $this->shortcode_atts['max_width'];
                                $max_width_tablet     = $this->shortcode_atts['max_width_tablet'];
                                $max_width_phone      = $this->shortcode_atts['max_width_phone'];
            
																$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
																
																if ( '' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width ) {
                                                $max_width_values = array(
                                                  'desktop' => $max_width,
                                                  'tablet'  => $max_width_tablet,
                                                  'phone'   => $max_width_phone,
                                                );
                          
                                                et_pb_generate_responsive_css( $max_width_values, '%%order_class%%', 'max-width', $function_name );
                                }
                          
                                if ( is_rtl() && 'left' === $text_orientation ) {
                                                $text_orientation = 'right';
                                }
                                
                                $desc = get_queried_object();
                                
                                if (!$avatar_size = $this->shortcode_atts['avatar_size']) {
                                            $avatar_size = 128;
                                }
                        
                                //////////////////////////////////////////////////////////////////////
                      
                                if (is_author()) {
                                          
                                                if ($show_avatar == 'on') {
                                                                $content .= '<div class="avatar_container">' . get_avatar(get_the_author_meta('id'), $avatar_size) . '</div>';
                                                }
                                                
                                                
                                                $content .= '<h1 itemprop="name" class="tax_title page_title entry-title">' . get_the_author() . '</h1>';
                                                
                                                if ($show_desc == 'on') {
                                                                if (!$description = $desc->category_description) {
                                                                                $description = $desc->description;
                                                                }

                                                                if ($description) {
                                                                        $content .= wpautop($description);
                                                                }
                                                }
                                          
                                } else if (is_date()) {
                                                if ( is_day() ) {
                                                                $title = sprintf( __( 'Daily Archives: %s', 'twentytwelve' ), '<span>' . get_the_date() . '</span>' );
                                                } else if ( is_month() ) {
                                                                $title = sprintf( __( 'Monthly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentytwelve' ) ) . '</span>' );
                                                } else if ( is_year() ) {
                                                                $title = sprintf( __( 'Yearly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentytwelve' ) ) . '</span>' );
                                                }
                                                
                                                $content .= '<h1 itemprop="name" class="tax_title page_title entry-title">' . $title . '</h1>';
                                                
                                } else {
                                          $content = '<h1 itemprop="name" class="tax_title page_title entry-title">' . single_term_title('', false) . '</h1>';
                                          
                                                if ($show_desc == 'on') {
                                                                if (!$description = $desc->category_description) {
                                                                                $description = $desc->description;
                                                                }

                                                                if ($description) {
                                                                        $content .= wpautop($description);
                                                                }
                                                }
                                }
                                
                                //////////////////////////////////////////////////////////////////////
                       
                                $output = sprintf(
                                                '<div%5$s class="%1$s%3$s%6$s">
                                                    %2$s
                                                %4$s',
                                                'clearfix ',
                                                $content,
                                                esc_attr( 'et_pb_module et_pb_tax_title et_pb_bg_layout_' . $background_layout . ' et_pb_text_align_' . $text_orientation ),
                                                '</div>',
                                                ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                                                ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
                                );
                      
                                return $output;
                }
            }
        
            new sb_et_tax_li_tax_title_module();

?>