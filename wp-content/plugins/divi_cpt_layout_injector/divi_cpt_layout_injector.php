<?php

/*
 * Plugin Name: CPT Layout Injector
 * Plugin URI:  http://www.sean-barton.co.uk
 * Description: A plugin to handle the layouts of any Custom Post Type pages using the ET layout builder system. 
 * Author:      Sean Barton - Tortoise IT
 * Version:     3.0
 * Author URI:  http://www.sean-barton.co.uk
 *
 * Changelog:
 *
 * V2.3
 * - Added taxonomy module for the Divi Builder
 * 
 * V2.4
 * - Added postnav module for next/prev navigation on post type items
 *
 * V2.5
 * - Added meta, author, comment and category settings to the CPT Post Title module for greater ease of use and flexibility
 *
 * V2.6
 * - Fixed taxonomy module issues
 * - Reversed options in title module for usability
 * 
 * V2.7
 * - Added author bio module
 * 
 * V2.8
 * - Added support for the 'Events Manager' plugin in the loop archive module
 *
 * V2.9
 * - Added better support for the 'page' post type
 *
 * V3.0 - 3/2/17
 * - Added text colour and alignment options to title and content modules
 * - added licensing and automatic updates
 * - improved settings page by adding helpful links when no layouts are present
 * - improved settings page by removing prebuilt layouts from the various layout dropdowns
 * - Loop Archive module, hide grid columns option when list mode selected
 *
 */

    require_once('includes/emp-licensing.php');
 
    add_action('plugins_loaded', 'sb_et_cpt_li_init');
    
    define('SB_ET_CPT_LI_VERSION', '3.0');
    
    function sb_et_cpt_li_init() {
    
        add_action('admin_menu', 'sb_et_cpt_li_submenu');
        add_action('init', 'sb_et_cpt_li_theme_setup', 9999);
        add_action('admin_head', 'sb_et_cpt_li_admin_head', 9999);
        add_action('wp_enqueue_scripts', 'sb_et_cpt_li_enqueue', 9999);
        
        add_filter( 'template_include', 'sb_et_cpt_li_template', 99 );
        add_filter('user_contactmethods','sb_et_cpt_li_contactmethods',10,1);
        
        add_image_size('sb_cpt_li_150_square', 150, 150, true);
        add_image_size('sb_cpt_li_250_square', 250, 250, true);
        add_image_size('sb_cpt_li_350_square', 350, 350, true);
        add_image_size('sb_cpt_li_150_wide', 150, false, true);
        add_image_size('sb_cpt_li_250_wide', 250, false, true);
        add_image_size('sb_cpt_li_350_wide', 350, false, true);
    }
    
    function sb_et_cpt_li_contactmethods( $contactmethods ) {
        $contactmethods['twitter'] = 'Twitter';
        $contactmethods['facebook'] = 'Facebook';
        $contactmethods['google_plus'] = 'Google+';
        
        return $contactmethods;
    }
    
    function sb_et_cpt_li_enqueue() {
        wp_enqueue_style('sb_et_cpt_li_css', plugins_url( '/style.css', __FILE__ ));
    }
    
    function sb_et_cpt_li_admin_head() {
        
        $post_types = md5(serialize(get_post_types()));
        $purge_cache = get_option('sb_et_cpt_li_builder_purge_cache', '');
        
        if ((isset($_GET['post_type']) && $_GET['post_type'] == 'et_pb_layout') || stripos($_SERVER['PHP_SELF'], 'wp-admin/index.php') !== false || $post_types != $purge_cache || isset($_GET['sb_purge_cache'])) {
            
            update_option('sb_et_cpt_li_builder_purge_cache', $post_types); //update purge cache
            
            $prop_to_remove = array(
                'et_pb_templates_et_pb_cpt_archive'
                , 'et_pb_templates_et_pb_cpt_loop_archive'
                , 'et_pb_templates_et_pb_cpt_text'
                , 'et_pb_templates_et_pb_cpt_author_bio'
                , 'et_pb_templates_et_pb_cpt_taxonomy'
                , 'et_pb_templates_et_pb_cpt_featured_image2'
                , 'et_pb_templates_et_pb_cpt_title'
                , 'et_pb_templates_et_pb_cpt_postnav'
            );
            
            $js_prop_to_remove = 'var sb_ls_remove = ["' . implode('","', $prop_to_remove) . '"];';
    
            echo '<script>
            
            ' . $js_prop_to_remove . '
            
            for (var prop in localStorage) {
                if (sb_ls_remove.indexOf(prop) != -1) {
                    console.log(localStorage.removeItem(prop));
                }
            }
            
            </script>';
        }
    }    
    
    function sb_et_cpt_li_template( $template ) {
        
        if (function_exists('et_pb_is_pagebuilder_used')) {
            $ps = get_post_status(get_the_ID());
            $pt = get_post_type();
            $meta_key = '';
            $is_page_builder_used = false;
            
            if (is_single() || is_page()) {
                $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
                $meta_key = 'sb_et_cpt_li_' . $pt . '_layout';
            } else if (is_archive()) {
                $meta_key = 'sb_et_cpt_li_' . $pt . '_archive_layout';
            }
                
            if ($meta_key && !$is_page_builder_used) {
                if ($ps && $ps != 'trash') {
                    //as in if it's anything it means it's not deleted and not equal to trash is obvious
                    if ($page_layout = get_option($meta_key)) {
                        $template = dirname(__FILE__) . '/empty.php'; //calls our own file which is a wrapper on a simple shortcode call
                    }
                }
            }
        }
    
        return $template;
    }
    
    function sb_et_cpt_li_single_template() {
        get_header();
        
        $pt = get_post_type();
        
        if (is_single() || is_page()) {
            $meta_key = 'sb_et_cpt_li_' . $pt . '_layout';
        } else if (is_archive()) {
            $meta_key = 'sb_et_cpt_li_' . $pt . '_archive_layout';
        }
        
        $cpt_layout = get_option($meta_key);
        
        if ($cpt_layout) {
            if ($section = do_shortcode('[et_pb_section global_module="' . $cpt_layout . '"][/et_pb_section]')) {
                echo '<div id="main-content">';
                echo $section;
                echo '</div>';
            }
        }
        
        get_footer();
    }
 
    function sb_et_cpt_li_submenu() {
        add_submenu_page(
            'options-general.php',
            'CPT Layout Injector',
            'CPT Layout Injector',
            'manage_options',
            'sb_et_cpt_li',
            'sb_et_cpt_li_submenu_cb' );
    }
    
    function sb_et_cpt_li_box_start($title) {
        return '<div class="postbox">
                    <h2 class="hndle">' . $title . '</h2>
                    <div class="inside">';
    }
    
    function sb_et_cpt_li_box_end() {
        return '    <div style="display: table; clear: both;">&nbsp;</div></div>
                </div>';
    }
     
    function sb_et_cpt_li_submenu_cb() {
        
        echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
        echo '<h2>CPT Layout Injector - V' . SB_ET_CPT_LI_VERSION . '</h2>';
        
        echo '<div id="poststuff">';
        
        echo '<div id="post-body" class="metabox-holder columns-2">';
        
        $ignored_types = array('attachment', 'product', 'download');
        
        $types = get_post_types();
        
        if (isset($_POST['sb_et_cpt_li_edit_submit'])) {
                foreach ($types as $type) {
                    update_option('sb_et_cpt_li_' . $type . '_layout', $_POST['sb_et_cpt_li_' . $type . '_layout']);
                    
                    if (isset($_POST['sb_et_cpt_li_' . $type . '_archive_layout'])) {
                        update_option('sb_et_cpt_li_' . $type . '_archive_layout', $_POST['sb_et_cpt_li_' . $type . '_archive_layout']);
                    }
                }
                
                echo '<div id="message" class="updated fade"><p>Layouts edited successfully</p></div>';
        }
        echo '<form method="POST">';
        
        sb_et_cpt_li_license_page();
        
        $layout_query = array(
            'post_type'=>'et_pb_layout'
            , 'posts_per_page'=>-1
            , 'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );
        
        echo sb_et_cpt_li_box_start('CPT Layout Injector');
        
        echo '<div class="alignright"><iframe width="300" height="169" src="https://www.youtube.com/embed/hMMWhi2sIlI?rel=0" frameborder="0" allowfullscreen></iframe></div>';
        
        echo '<p>This plugin allows you to edit the layouts of your Divi site without having to edit any core files.</p>';
        echo '<p>A layout can be built within the Divi/Extra library using the page builder. You can then use these settings to set the appropriate layout to the Custom Post Type page(s). You\'ll need to include a variety of new modules in the page builder to make the page work. This plugin will do the rest!</p><p>If you need any support for this plugin please visit the documentation website at <a href="http://docs.tortoise-it.co.uk/cpt-layout-injector/" target="_blank">docs.tortoise-it.co.uk/cpt-layout-injector/</a>.</p><p>The following video will explain the loop archive module which is integral to this plugin.</p>';
        
        echo sb_et_cpt_li_box_end();
        
        echo '<div style="display: inline-block; margin-right: 10px; margin-bottom: 20px; padding: 5px 0px;">Jump to:</div>';
                
        foreach ($types as $type2) {
            $type_obj2 = get_post_type_object($type2);
            
            if (!$type_obj2->public) {
                continue;
            }
            if (in_array($type2, $ignored_types)) {
                continue;
            }
            
            $type_name2 = $type_obj2->labels->name;
            
            echo '<div style="display: inline-block; margin-right: 10px; margin-bottom: 20px; padding: 5px 10px; background: #333"><a style="color: white; text-decoration: none; " href="#' . $type2 . '">' . $type_name2 . '</a></div>';
        }
        
        if ($layouts = get_posts($layout_query)) {
            
            foreach ($types as $type) {
                $type_obj = get_post_type_object($type);
                
                if (!$type_obj->public) {
                    continue;
                }
                if (in_array($type, $ignored_types)) {
                    continue;
                }
                
                $type_name = $type_obj->labels->name;
                
                echo '<a name="' . $type . '"></a>';
                echo sb_et_cpt_li_box_start($type_name . ' Layout');
            
                echo '<label style="display:inline-block; min-width: 200px;">Single Template: </label><select name="sb_et_cpt_li_' . $type . '_layout">';
                
                $cpt_layout = get_option('sb_et_cpt_li_' . $type . '_layout');
                
                echo '<option value="">-- None --</option>';
                
                foreach ($layouts as $layout) {
                    echo '<option ' . selected($layout->ID, $cpt_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                }
                
                echo '</select>';
                echo '<br />';
                if ($type_obj->has_archive) {
                    $url = get_post_type_archive_link($type);
                    
                    echo '<label style="display:inline-block; min-width: 200px;">Archive Template: </label><select name="sb_et_cpt_li_' . $type . '_archive_layout">';
                    
                    $cpt_archive_layout = get_option('sb_et_cpt_li_' . $type . '_archive_layout');
                    
                    echo '<option value="">-- None --</option>';
                    
                    foreach ($layouts as $layout) {
                        echo '<option ' . selected($layout->ID, $cpt_archive_layout, false) . ' value="' . $layout->ID . '">' . $layout->post_title . '</option>';
                    }
                    
                    echo '</select>';
                    
                    echo '<p>You can see your post type archive at <a target="_blank" href="' . $url . '">' . $url . '</a></p>';
                } else {
                    if ($type == 'post') {
                        echo '<p>Divi doesn\'t have an archive page in the conventional sense. Normally you would simply visit the posts page and see a list of blogs however, using Divi, we are encouraged to use the "Blog" module to make a grid or list page containing the latest items. I agree with this and, as such, encourage you to create a "Page" called "Blog" or similar and use the "CPT Loop Archive" Module. The only difference is that you need to select "Yes" for "Custom Query" and "Post" as the "Post Type". Pages 2 onwards will use the same template. If you want to style your category/tag pages you\'ll need the <a href="https://elegantmarketplace.com/downloads/taxonomy-layout-injector/" target="_blank">Taxonomy Layout Injector</a> plugin</p>';
                    } else if ($type == 'page') {
                        echo '<p>There is no real reason to have an archive of Pages. If you feel you need one please contact me at <a href="https://www.sean-barton.co.uk">sean-barton.co.uk</a> and I would be more than happy to help you structure your site and use the appropriate data type(s)</p>';
                    } else {
                        echo '<p>This post type has has_archive set to false meaning you can see the single post type item pages but there is no concept of an archive or view showing a group of items at once. This is not recommended. Once you have turned on has_archive then you will be able to inject a layout using this page.</p>';
                    }
                }
                
                echo sb_et_cpt_li_box_end();
                
            }
            
            echo '<p id="submit"><input type="submit" name="sb_et_cpt_li_edit_submit" class="button-primary" value="Save Settings" /></p>';
        } else {
            echo '<div style="padding:100px; border: 2px solid #999; text-align: center;">
                    <h1>Oops no layouts!</h1>
                    <p style="font-size: 16px;">Please visit the Divi Library to add your first layout and then this page will become available</p>
                    <p><a href="' . (admin_url('/edit.php?post_type=et_pb_layout')) . '" style="display: inline-block; padding: 10px 30px; border: 1px solid #999; font-size: 16px; background-color: #333; color: white; font-weight: bold; border-radius: 20px; text-decoration: none;">Click here to visit the Divi Library</a></p>
                </div>';
        }
        
        echo '</form>';
            
        echo '</div>';
        
        echo '</div>';
        echo '</div>';
    }
    
    function sb_et_cpt_li_get_taxonomies() {
            $return = array();
                    
            $ignored = array(
                'nav_menu'
                , 'post_format'
                , 'link_category'
            );
            
            $ignored_stubs = array(
                'pa_'
                , 'product_'
            );
            
            if ($taxonomies = get_taxonomies(false, 'object')) {
                foreach ( $taxonomies  as $taxonomy ) {
                    
                    if ($taxonomy->publicly_queryable && !in_array($taxonomy->name, $ignored)) {
                        $ignore_this = false;
                        
                        foreach ($ignored_stubs as $ignored_stub) {
                            $length = strlen($ignored_stub);
                            if (substr($taxonomy->name, 0, $length) == $ignored_stub) {
                                $ignore_this = true;
                                break;
                            }
                        }
                    
                        if (!$ignore_this) {
                            if (isset($taxonomy->name)) {
                                $return[$taxonomy->name] = $taxonomy->label;
                            }
                        }
                    }
                }
            }
            
            return $return;
    }
    
    function sb_et_cpt_li_theme_setup() {
    
        if ( class_exists('ET_Builder_Module')) {
        
            $modules_path = trailingslashit(dirname(__FILE__)) . 'modules/';
            
            require_once($modules_path . 'sb_et_cpt_li_title_module.php');
            require_once($modules_path . 'sb_et_cpt_li_author_bio_module.php');
            require_once($modules_path . 'sb_et_cpt_li_postnav_module.php');
            require_once($modules_path . 'sb_et_cpt_li_taxonomy_module.php');
            require_once($modules_path . 'sb_et_cpt_li_content_module.php');
            require_once($modules_path . 'sb_et_cpt_li_gallery_module.php');
            require_once($modules_path . 'sb_et_cpt_li_pt_archive_module.php');
            require_once($modules_path . 'sb_et_cpt_li_loop_archive_module.php');
            
        }
    }
 
?>