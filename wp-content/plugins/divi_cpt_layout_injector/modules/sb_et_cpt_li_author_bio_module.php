<?php

class sb_et_cpt_li_author_bio_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'ET CPT Author Bio', 'et_builder' );
                    $this->slug = 'et_pb_cpt_author_bio';
            
                    $this->whitelisted_fields = array(
                        'module_id',
                        'module_class',
												'author_name',
												'author_name_link',
												'author_name_before',
												'author_bio',
												'author_website',
												'author_website_before',
												'author_social',
												'author_avatar',
												'author_avatar_size',
												'author_avatar_alignment',
                    );
            
                    $this->fields_defaults = array();
                    $this->main_css_element = '%%order_class%%';
                    
																$this->advanced_options = array(
                                        'fonts' => array(
                                                'text'   => array(
                                                                'label'    => esc_html__( 'Text', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} p",
                                                                ),
                                                                'font_size' => array('default' => '14px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                                'headings'   => array(
                                                                'label'    => esc_html__( 'Headings', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} h1, {$this->main_css_element} h2, {$this->main_css_element} h1 a, {$this->main_css_element} h2 a, {$this->main_css_element} h1 a, {$this->main_css_element} h2 a, {$this->main_css_element} h3, {$this->main_css_element} h4",
                                                                ),
                                                                'font_size' => array('default' => '30px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                                'social_media'   => array(
                                                                'label'    => esc_html__( 'Social Media Icons', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} .sb_et_cpt_li_author_social .et-social-icons a",
                                                                ),
                                                                'font_size' => array('default' => '24px'),
                                                                'line_height'    => array('default' => '1.2em'),
                                                ),
                                        ),
                                        'background' => array(
                                                'settings' => array(
                                                        'color' => 'alpha',
                                                ),
                                        ),
                                        'border' => array(),
                                        'custom_margin_padding' => array(
                                                'css' => array(
                                                        'important' => 'all',
                                                ),
                                        ),
                                );
                }
            
                function get_fields() {
                    $fields = array(
																'admin_label' => array(
																								'label'       => __( 'Admin Label', 'et_builder' ),
																								'type'        => 'text',
																								'description' => __( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
																),
																'module_id' => array(
																								'label'           => esc_html__( 'CSS ID', 'et_builder' ),
																								'type'            => 'text',
																								'option_category' => 'configuration',
																								'tab_slug'        => 'custom_css',
																								'option_class'    => 'et_pb_custom_css_regular',
																),
																'module_class' => array(
																								'label'           => esc_html__( 'CSS Class', 'et_builder' ),
																								'type'            => 'text',
																								'option_category' => 'configuration',
																								'tab_slug'        => 'custom_css',
																								'option_class'    => 'et_pb_custom_css_regular',
																),
																'author_name' => array(
																								'label'             => esc_html__( 'Show Author Name', 'et_builder' ),
																								'type'              => 'yes_no_button',
																								'option_category'   => 'configuration',
																								'options'           => array(
																																'on'  => esc_html__( 'Yes', 'et_builder' ),
																																'off' => esc_html__( 'No', 'et_builder' ),
																								),
																								'affects'           => array(
																																'author_name_link',
																																'author_name_target',
																																'author_name_before',
																																//'author_name_after',
																								),
																),
																'author_name_link' => array(
																								'label'             => esc_html__( 'Link Author Name', 'et_builder' ),
																								'type'              => 'select',
																								'options'         => array(
																									'author_url' => esc_html__( 'Author URL (automatic WP archive)', 'et_builder' ),
																									'user_url'  => esc_html__( 'User URL (set in profile)', 'et_builder' ),
																									'none'  => esc_html__( 'No link', 'et_builder' ),
																								),
																								'option_category'   => 'configuration',
																								'depends_show_if'   => 'on',
																),
																'author_name_target' => array(
																								'label'             => esc_html__( 'Link Target', 'et_builder' ),
																								'type'              => 'select',
																								'options'         => array(
																									'none' => 'None',
																									'blank'  => 'Blank',
																								),
																								'option_category'   => 'configuration',
																								'depends_show_if'   => 'on',
																),
																'author_name_before' => array(
																								'label'             => esc_html__( 'Pre Text', 'et_builder' ),
																								'type'              => 'text',
																								'option_category'   => 'configuration',
																								'depends_show_if'   => 'on',
																								'description'				=> 'The text to show before this item.. (eg: About the author...)'
																),
																'author_bio' => array(
																								'label'             => esc_html__( 'Show Author Bio', 'et_builder' ),
																								'type'              => 'yes_no_button',
																								'option_category'   => 'configuration',
																								'options'           => array(
																																'on'  => esc_html__( 'Yes', 'et_builder' ),
																																'off' => esc_html__( 'No', 'et_builder' ),
																								),
																),
																'author_website' => array(
																								'label'             => esc_html__( 'Show Author Website', 'et_builder' ),
																								'type'              => 'yes_no_button',
																								'option_category'   => 'configuration',
																								'options'           => array(
																																'on'  => esc_html__( 'Yes', 'et_builder' ),
																																'off' => esc_html__( 'No', 'et_builder' ),
																								),
																								'affects'           => array(
																																'author_website_before',
																								),
																),
																'author_website_before' => array(
																								'label'             => esc_html__( 'Pre Text', 'et_builder' ),
																								'type'              => 'text',
																								'option_category'   => 'configuration',
																								'depends_show_if'   => 'on',
																								'description'				=> 'The text to show before this item.. (eg: Author website...)'
																),
																'author_social' => array(
																								'label'             => esc_html__( 'Show Author Social Media', 'et_builder' ),
																								'type'              => 'yes_no_button',
																								'option_category'   => 'configuration',
																								'options'           => array(
																																'on'  => esc_html__( 'Yes', 'et_builder' ),
																																'off' => esc_html__( 'No', 'et_builder' ),
																								),
																),
																'author_avatar' => array(
																								'label'             => esc_html__( 'Show Author Avatar', 'et_builder' ),
																								'type'              => 'yes_no_button',
																								'option_category'   => 'configuration',
																								'options'           => array(
																																'on'  => esc_html__( 'Yes', 'et_builder' ),
																																'off' => esc_html__( 'No', 'et_builder' ),
																								),
																								'affects'           => array(
																																'author_avatar_size',
																																'author_avatar_alignment',
																								),
																),
																'author_avatar_size' => array(
																								'label'             => esc_html__( 'Avatar Size', 'et_builder' ),
																								'type'              => 'text',
																								'option_category'   => 'configuration',
																								'depends_show_if'   => 'on',
																								'description'       => esc_html__( 'The size in pixels of the avatar. Defaults to 128. Enter a number ONLY. 64 being small, 256 being large.', 'et_builder' ),
																),
																'author_avatar_alignment' => array(
																								'label'             => esc_html__( 'Avatar Alignment', 'et_builder' ),
																								'type'              => 'select',
																								'options'         => array(
																									'alignleft' => esc_html__( 'Left', 'et_builder' ),
																									'alignright'  => esc_html__( 'Right', 'et_builder' ),
																								),
																								'depends_show_if'   => 'on',
																								'description'       => esc_html__( 'Alignment of the avatar', 'et_builder' ),
																),
                    );
                    
                    return $fields;
                }
            
                function shortcode_callback( $atts, $content = null, $function_name ) {
																$module_id = $this->shortcode_atts['module_id'];
																$module_class = $this->shortcode_atts['module_class'];
																$author_name = $this->shortcode_atts['author_name'];
																$author_name_before = $this->shortcode_atts['author_name_before'];
																$author_name_link = $this->shortcode_atts['author_name_link'];
																$author_name_target = $this->shortcode_atts['author_name_target'];
																$author_bio = $this->shortcode_atts['author_bio'];
																$author_website = $this->shortcode_atts['author_website'];
																$author_website_before = $this->shortcode_atts['author_website_before'];
																$author_social = $this->shortcode_atts['author_social'];
																$author_avatar = $this->shortcode_atts['author_avatar'];
																$author_avatar = $this->shortcode_atts['author_avatar'];
																$author_avatar_size = $this->shortcode_atts['author_avatar_size'];
																$author_avatar_alignment = $this->shortcode_atts['author_avatar_alignment'];
																
																$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
            
																//////////////////////////////////////////////////////////////////////

																$output = '';
														
																if ( $author_name == 'on' ) {
																								//print_r($author_name_link);
																								
																								$url = '';
																								switch ($author_name_link) {
																																case 'author_url':
																																								$url = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );
																																								break;
																																case 'user_url':
																																								$url = get_the_author_meta( 'url' );
																																								break;
																																case 'none':
																																								$url = '';
																																								break;
																								}
																								
																								$output .= '<h3 class="author_archive_title">' . ($author_name_before ? $author_name_before . ' ':'') . ($url ? '<a ' . ($author_name_target == 'blank' ? 'target="_blank"':'') . ' href="' . $url . '">':'') . get_the_author_meta( 'display_name' ) . ($url ? '</a>':'') . '</h3>';
																}
																
																if ( $author_avatar == 'on' ) {
																								if (!$author_avatar_size || !is_numeric($author_avatar_size)) {
																																$author_avatar_size = 128;
																								}
																								
																								if ($avatar = get_avatar( get_the_author_meta( 'ID' ), $author_avatar_size, false, false, array('class'=>$author_avatar_alignment) )) {
																																$output .= $avatar;
																								}
																}
																
																if ( $author_bio == 'on' ) {
																								$output .= '<p class="sb_et_cpt_li_author_bio">' . get_the_author_meta( 'description' ) . '</p>';
																}
																
																if ( $author_social == 'on' ) {
																								$fb = get_the_author_meta( 'facebook' );
																								$tw = get_the_author_meta( 'twitter' );
																								$gp = get_the_author_meta( 'google_plus' );
																								
																								if ($fb || $tw || $gp) {
																								
																																$output .= '<span class="sb_et_cpt_li_author_social">';
																																
																																$output .= '<ul class="et-social-icons">';
								
																																if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) {
																																								if ($fb) {
																																																$output .= '<li class="et-social-icon et-social-facebook">
																																																								<a href="' . esc_url($url) . '" class="icon">
																																																									<span>' . esc_html( 'Facebook', 'Divi' ) . '</span>
																																																								</a>
																																																</li>';
																																								}
																																}
																																if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) {
																																								if ($tw) {
																																																$output .= '<li class="et-social-icon et-social-twitter">
																																																								<a href="' . esc_url($url) . '" class="icon">
																																																									<span>' . esc_html( 'Twitter', 'Divi' ) . '</span>
																																																								</a>
																																																</li>';
																																								}
																																}
																																if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) {
																																								if ($gp) {
																																																$output .= '<li class="et-social-icon et-social-google-plus">
																																																								<a href="' . esc_url($url) . '" class="icon">
																																																									<span>' . esc_html( 'Google', 'Divi' ) . '</span>
																																																								</a>
																																																</li>';
																																								}
																																}
																																
																																$output .= '</ul>';
																																
																																$output .= '</span>';
																								}
																}
																
																if ( $author_website == 'on' ) {
																								if ($url = get_the_author_meta( 'url' )) {
																																$output .= '<p class="sb_et_cpt_li_author_link">' . ($author_website_before ? $author_website_before . ' ':'') . '<a href="' . $url . '" target="_blank">' . $url . '</a></p>';
																								}
																}
														
																//////////////////////////////////////////////////////////////////////
            
																if ($output) {
																		$output = sprintf(
																				'<div%5$s class="%1$s%3$s%6$s">
																						%2$s
																				%4$s',
																				'clearfix ',
																				$output,
																				esc_attr( 'et_pb_module' ),
																				'</div>',
																				( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
																				( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
																		);
																}
            
                    return $output;
                }
            }
        
            new sb_et_cpt_li_author_bio_module();

?>