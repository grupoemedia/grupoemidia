<?php

class sb_et_cpt_li_taxonomy_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'ET CPT Taxonomy', 'et_builder' );
                    $this->slug = 'et_pb_cpt_taxonomy';
            
                    $this->whitelisted_fields = array(
																'taxonomy',
																'separator',
																'module_id',
																'module_class',
                    );
            
                    $this->fields_defaults = array();
                    //$this->main_css_element = '.et_pb_cpt_title';
                    //$this->advanced_options = array();
                    //$this->custom_css_options = array();
                    
                    $this->main_css_element = '%%order_class%%';
                    
																$this->advanced_options = array(
                                        'fonts' => array(
                                                'text'   => array(
                                                                'label'    => esc_html__( 'Text', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} p",
                                                                ),
                                                                'font_size' => array('default' => '14px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                                'headings'   => array(
                                                                'label'    => esc_html__( 'Headings', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} h1, {$this->main_css_element} h2, {$this->main_css_element} h1 a, {$this->main_css_element} h2 a, {$this->main_css_element} h3, {$this->main_css_element} h4",
                                                                ),
                                                                'font_size' => array('default' => '30px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                        ),
                                        'background' => array(
                                                'settings' => array(
                                                        'color' => 'alpha',
                                                ),
                                        ),
                                        'border' => array(),
                                        'custom_margin_padding' => array(
                                                'css' => array(
                                                        'important' => 'all',
                                                ),
                                        ),
                                );
                }
            
                function get_fields() {
																$tax = sb_et_cpt_li_get_taxonomies();
																
																$fields = array(
																		'admin_label' => array(
																				'label'       => __( 'Admin Label', 'et_builder' ),
																				'type'        => 'text',
																				'description' => __( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
																		),
																		'taxonomy' => array(
																						'label'           => esc_html__( 'Taxonomy', 'et_builder' ),
																						'type'            => 'select',
																						'options'=>$tax,
																						'description'=>'Which taxonomy should the system check against. This will display a list of links to categories etc that this post is tagged against'
																					),
																		'separator' => array(
																						'label'           => esc_html__( 'Separator', 'et_builder' ),
																						'type'            => 'text',
																						'description'    => 'When there is more than one term to display what should separate them. Eg | or ,',
																					),
																		'module_id' => array(
																						'label'           => esc_html__( 'CSS ID', 'et_builder' ),
																						'type'            => 'text',
																						'option_category' => 'configuration',
																						'tab_slug'        => 'custom_css',
																						'option_class'    => 'et_pb_custom_css_regular',
																					),
																					'module_class' => array(
																						'label'           => esc_html__( 'CSS Class', 'et_builder' ),
																						'type'            => 'text',
																						'option_category' => 'configuration',
																						'tab_slug'        => 'custom_css',
																						'option_class'    => 'et_pb_custom_css_regular',
																					),
																);
																
																//print_r($fields);
																
																return $fields;
                }
            
                function shortcode_callback( $atts, $content = null, $function_name ) {
                    $module_id          = $this->shortcode_atts['module_id'];
                    $module_class       = $this->shortcode_atts['module_class'];
                    $taxonomy       = $this->shortcode_atts['taxonomy'];
                    $separator       = $this->shortcode_atts['separator'];
										$output = $content = '';
            
                    $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
            
                    //////////////////////////////////////////////////////////////////////
                      
																$product_terms = wp_get_object_terms( get_the_ID(),  $taxonomy );
																$term_array = array();
																
																if ( ! empty( $product_terms ) ) {
																								if ( ! is_wp_error( $product_terms ) ) {
																																foreach( $product_terms as $term ) {
																																								$term_array[] = '<a href="' . get_term_link( $term->slug, $taxonomy ) . '">' . esc_html( $term->name ) . '</a>';
																																}
																							
																																$content = '<span class="sb_cpt_term_list">';
																																$content .= implode($separator, $term_array);
																																$content .= '</span>';
																								}
																}
                      
                     //////////////////////////////////////////////////////////////////////
																
																if ($content) {
																				$output = sprintf(
																						'<div%5$s class="%1$s%3$s%6$s">
																								%2$s
																						%4$s',
																						'clearfix ',
																						$content,
																						esc_attr( 'et_pb_module' ),
																						'</div>',
																						( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
																						( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
																				);
																}
            
                    return $output;
                }
            }
        
            new sb_et_cpt_li_taxonomy_module();

?>