<?php

class sb_et_cpt_li_postnav_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'ET CPT Post Nav', 'et_builder' );
                    $this->slug = 'et_pb_cpt_postnav';
            
                    $this->whitelisted_fields = array(
                        'module_id',
                        'module_class',
                    );
            
                    $this->fields_defaults = array();
                    
                    $this->main_css_element = '%%order_class%%';
                    
																$this->advanced_options = array(
                                        'fonts' => array(
                                                'text'   => array(
                                                                'label'    => esc_html__( 'Text', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} .sb_pb_pagination a, {$this->main_css_element} .sb_pb_pagination span, {$this->main_css_element} p",
                                                                ),
                                                                'font_size' => array('default' => '14px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                                'headings'   => array(
                                                                'label'    => esc_html__( 'Headings', 'et_builder' ),
                                                                'css'      => array(
                                                                        'main' => "{$this->main_css_element} h1, {$this->main_css_element} h2, {$this->main_css_element} h3, {$this->main_css_element} h4",
                                                                ),
                                                                'font_size' => array('default' => '30px'),
                                                                'line_height'    => array('default' => '1.5em'),
                                                ),
                                        ),
                                        'background' => array(
                                                'settings' => array(
                                                        'color' => 'alpha',
                                                ),
                                        ),
                                        'border' => array(),
                                        'custom_margin_padding' => array(
                                                'css' => array(
                                                        'important' => 'all',
                                                ),
                                        ),
                                );
                }
            
                function get_fields() {
                    $fields = array(
                        'admin_label' => array(
                            'label'       => __( 'Admin Label', 'et_builder' ),
                            'type'        => 'text',
                            'description' => __( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
                        ),
                        'module_id' => array(
																'label'           => esc_html__( 'CSS ID', 'et_builder' ),
																'type'            => 'text',
																'option_category' => 'configuration',
																'tab_slug'        => 'custom_css',
																'option_class'    => 'et_pb_custom_css_regular',
															),
															'module_class' => array(
																'label'           => esc_html__( 'CSS Class', 'et_builder' ),
																'type'            => 'text',
																'option_category' => 'configuration',
																'tab_slug'        => 'custom_css',
																'option_class'    => 'et_pb_custom_css_regular',
															)
                    );
                    
                    return $fields;
                }
            
                function shortcode_callback( $atts, $content = null, $function_name ) {
                    $module_id          = $this->shortcode_atts['module_id'];
                    $module_class       = $this->shortcode_atts['module_class'];
            
                    $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
            
                    //////////////////////////////////////////////////////////////////////
																ob_start();
																?>
																<div class="sb_pb_pagination pagination clearfix">
																								<div class="alignleft"><?php previous_post_link(); ?></div>
																								<div class="alignright"><?php next_post_link(); ?></div>
																</div>
																<?php
																$content = ob_get_clean();
                     //////////////////////////////////////////////////////////////////////
            
                    $output = sprintf(
                        '<div%5$s class="%1$s%3$s%6$s">
                            %2$s
                        %4$s',
                        'clearfix ',
                        $content,
                        esc_attr( 'et_pb_module' ),
                        '</div>',
                        ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                        ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
                    );
            
                    return $output;
                }
            }
        
            new sb_et_cpt_li_postnav_module();

?>