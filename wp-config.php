<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'grupoemidia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VutF5|Z3AK<ZExg.(*#ka$H`[[c98)%H:1anWTc{*X:nSMCf]0P3<C_9w=vH,Ry(');
define('SECURE_AUTH_KEY',  'ML4?{yd=$gu{~-vM=6amhl!@s&l, Da70A-nt=7wg%?|@ClhH6_nBTT`<sPc,Tar');
define('LOGGED_IN_KEY',    'z[Xgn6E5M]nat!kwlpNO,r_YyBJCRNX`QtIqCR+I|uhmpjP<cH-g/W;MWlE-Cd4W');
define('NONCE_KEY',        '6Z~FzCtJd[65u@<I:-*B*K#XKge9k${Xbk4R t}ZTvxWZ,BN6=aY|Mzg(YOn6(KS');
define('AUTH_SALT',        'nZ<|0K2x,]W*4KTr-5IdR7> cZ@T~J!xSDepBM_G/%A4XwX)XfxvIHNP-B+mkhx8');
define('SECURE_AUTH_SALT', 'Ncn2wIR9p|L^#l;~qW^-7Yu*&G-zuE(wbpiH&+4gHQ08qiIZ$+Ku&Q#$5fPT3[JN');
define('LOGGED_IN_SALT',   'iXV%q$.^(@YM,lWP6wkH6lh6uz1}_=(6p v0[|~A?DBaw&nK/x{-wRVNy5Wt8SRK');
define('NONCE_SALT',       '~M7~4Qyjf-R,*F*Ylo(U)kyV,9>LuU:|Y(7*$?Yc^OofcWJvjdFatmMvOKIpx`3N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'e_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
